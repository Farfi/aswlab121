package test;

import com.thoughtworks.selenium.Selenium;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.regex.Pattern;
import static org.apache.commons.lang3.StringUtils.join;

public class seleniumTest_2 {
	private Selenium selenium;

	@Before
	public void setUp() throws Exception {
		WebDriver driver = new FirefoxDriver();
		String baseUrl = "http://localhost:8080/";
		selenium = new WebDriverBackedSelenium(driver, baseUrl);
	}

	@Test
	public void testSeleniumTest_2() throws Exception {
		 
		
		selenium.open("/aswlab12/");
		selenium.type("id=login_username", "test");
		selenium.type("id=login_password", "test");
		selenium.click("name=action");
		selenium.waitForPageToLoad("30000");
		selenium.type("id=tweet_content", "HolaHolaaa Pastillacas "+Math.random());
		selenium.click("css=div.publish > form > input[name=\"action\"]");
		selenium.waitForPageToLoad("30000");
		assertEquals("HolaHolaaa Pastillacas", selenium.getText("css=p"));
		selenium.click("name=action");
		selenium.waitForPageToLoad("30000");
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
