package test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import springFront.LogInOutController;


public class LoginLogoutTests {

	private MockMvc mockMvc;

	@Before
	public void initDatabase() {
		TestDB.startup();
		this.mockMvc = MockMvcBuilders.standaloneSetup(new LogInOutController()).build();

	}

	@After
	public void closeDatabase() {
		TestDB.shutdown();
	}

	@Test
	public void test1()  throws Exception{

		// Login OK
		this.mockMvc.perform(post("/login")
				.param("login_username", "test")
				.param("login_password", "test"))
				.andExpect(status().isMovedTemporarily())
				.andExpect(redirectedUrl("/"))
				.andExpect(request().sessionAttribute("loggedUserNAME", equalTo("test")));


	}

	@Test
	public void test2()  throws Exception {

		//fail("logout testing not yet implemented");
		//Login OK
		this.mockMvc.perform(post("/login")
				.param("login_username", "test")
				.param("login_password", "test"))
				.andExpect(status().isMovedTemporarily())
				.andExpect(redirectedUrl("/"))
				.andExpect(request().sessionAttribute("loggedUserNAME", equalTo("test")));

		
		// LogOut OK
		this.mockMvc.perform(post("/logout"))
				.andExpect(status().isMovedTemporarily())
				.andExpect(redirectedUrl("/"));

	}

	@Test
	public void test3()  throws Exception {

		//Login KO
		this.mockMvc.perform(post("/login")
				.param("login_username", "test")
				.param("login_password", "wrongpassword"))
				.andExpect(status().isOk())
				.andExpect(request().sessionAttribute("loggedUserNAME", nullValue()));


	}

}

