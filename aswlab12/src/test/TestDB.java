package test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import models.ActiveRecord;

public class TestDB    {

	// This application uses an embedded relational database
	// through the "H2 Database Engine (www.h2database.com)
	// The database in question, "walldb", is stored in the directory WEB-INF/db
	// The application uses a single JDBC connection 
	// that is stored as an attribute of the "ServletContext", so that all the servlets
	// of the application will share it.
	// In the file WEB-INF/web.xml H2dbStarter is configured as a "listener" so
	// that its method contextInitalized is called when the application is launched

	private static Connection dbConnection = null;

	public static void startup() {
		try {
			Class.forName(Parameters.getString("walldb.driver")); 
			String dbPath = TestDB.class.getProtectionDomain().getCodeSource().getLocation()+Parameters.getString("walldb.path"); 
			dbConnection = DriverManager.getConnection(Parameters.getString("walldb.uri")+dbPath, Parameters.getString("walldb.user"), Parameters.getString("walldb.password"));
			ActiveRecord.setDbConnection(dbConnection);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}




	public static Connection getDbConnection() {
		return dbConnection;
	}




	public static void shutdown() {
		ActiveRecord.setDbConnection(null);
		try {
			Statement stat = dbConnection.createStatement();
			stat.execute("SHUTDOWN");
			stat.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			dbConnection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			dbConnection = null;
		}

	}

}
